import 'package:get/get.dart';
import 'app/routes/app_pages.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(
    GetMaterialApp(
      debugShowCheckedModeBanner: false,
      title: "TO DO",
      initialRoute: AppPages.INITIAL,
      getPages: AppPages.routes,
    ),
  );
}
