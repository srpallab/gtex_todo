import 'package:get/get.dart';
import 'package:flutter/material.dart';

import '../../../core/theme/layout_theme.dart';
import '../controllers/home_controller.dart';

class HomeView extends GetView<HomeController> {
  const HomeView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return blankLayout(Container());
  }
}
